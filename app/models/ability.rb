# frozen_string_literal: true

class Ability # :nodoc:
  include CanCan::Ability

  def initialize(user)
    alias_action :create, :read, :update, :destroy, to: :crud

    user ||= User.new
    if user.admin?
      can :manage, :all
    else
      can :read, RoomMessage
      can :crud, RoomMessage, user_id: user.id
    end
  end
end